import React from 'react';
import Head from 'next/head';

import BirthdayCard from '../components/BirthdayCard';
import withData from '../lib/withData';

export default withData(() => (
  <div className="root">
    <Head>
      <title>The Overkill Birthday Card</title>
      <meta key="viewport" name="viewport" content="initial-scale=1.0, width=device-width" />
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,600,700" rel="stylesheet" />
    </Head>

    <header>
      <h1>The Overkill Birthday Card</h1>
      <p>
        A totally overkill, highly-optimized, server-rendered birthday card built
        with React and GraphQL for no reason.
      </p>
    </header>

    <main>
      <BirthdayCard />
    </main>

    <footer>
      <p>
        This is a birthday present made by <b>Alberto Restifo</b> for <b>Claudio Restifo</b>.
      </p>
      <p className="small">
        If you're not Claudio Restifo, get the fuck out. This is my corner of the
        internet.
      </p>
    </footer>

    <style jsx global>{`

      *, *::before, *::after {
        box-sizing: border-box;
      }

      html, body {
        margin: 0;
        padding: 0;
        min-height: 100%;
      }

      body {
        background: linear-gradient(to bottom, #1488cc, #2b32b2);
        font-family: 'Roboto', sans-serif;
        color: white;
      }

    `}</style>
    <style jsx>{`

      .root {
        height: 100vh;
        padding: 50px;
        display: grid;
        justify-items: center;
        grid-template-columns: 1fr;
        grid-template-rows: auto 1fr auto;
        grid-template-areas:
          "header"
          "main"
          "footer";
      }

      header {
        grid-area: header;
        margin: 3em 0;
        display: flex;
        flex-flow: column nowrap;
        align-items: center;
      }

      header h1 {
        font-size: 42px;
        font-weight: 600;
        text-transform: uppercase;
        letter-spacing: 0.1em;
        text-shadow: 0 2px 2px rgba(0, 0, 0, .24);
        margin-top: 0;
        margin-bottom: 0.25em;
        line-height: 1.5;
      }

      header p {
        line-height: 1.5;
        margin-top: 0;
        font-size: 16px;
        text-align: center;
        max-width: 50ch;
        margin-bottom: 0;
      }

      main {
        grid-area: main;
        max-width: 750px;
      }

      footer {
        grid-area: footer;
        line-height: 1.5;
        text-align: center;
        margin-top: 5em;
      }

      footer > p {
        margin-top: 0;
        margin-bottom: 0.25em;
        color: rgba(255, 255, 255, .75);
        font-size: 16px;
      }

      footer > p.small {
        font-size: 12px;
      }

    `}</style>
  </div>
));
