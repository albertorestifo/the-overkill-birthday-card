import React from 'react';

export default class BackgroundVideo extends React.PureComponent {

  render() {
    return (
      <div>
        <video
          src="/static/video.webm"
          loop
          autoPlay
        />
        <style jsx>{`
          video {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: -100;
            height: 100%;
          }
        `}</style>
      </div>
    );
  }

}
