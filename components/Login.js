import React from 'react';

import GoogleButton from './GoogleButton';

export default () => (
  <div className="container">

    <GoogleButton />

    <p>
      Please login to get your birthday wishes.
    </p>


    <style jsx>{`

      .container {
        display: flex;
        flex-flow: column nowrap;
        align-items: center;
        margin-top: 3em;
      }

      p {
        font-size: 14px;
      }

    `}</style>
  </div>
);
