import React from 'react';
import Link from 'next/link';

export default () => (
  <Link href="/oauth">
    <a>
      <img alt="Google Logo" src="/static/google-logo.svg" />

      <span className="text">
        Sign in with Google
      </span>

      <style jsx>{`

        a {
          display: flex;
          align-items: center;
          width: 185px;
          height: 40px;
          background-color: white;
          border: none;
          appearance: none;
          text-decoration: none;
          padding: 0;
          margin: 0;
          border-radius: 2px;
          padding-left: 11px;
          padding-right: 9px;
          font-size: 14px;
          font-family: 'Roboto', sans-serif;
          font-weight: 600;
          color: rgba(0, 0, 0, .54);
          cursor: pointer;
          box-shadow:
            0 0   1px rgba(0, 0, 0, .12),
            0 1px 1px rgba(0, 0, 0, .24);
        }

        a:focus, a:hover {
          box-shadow:
            0 0   2px rgba(0, 0, 0, .12),
            0 2px 2px rgba(0, 0, 0, .24);
        }

        a:active {
          background-color: #EEEEEE;
        }

        img {
          margin-right: 21px;
          line-height: 0;
        }

        .text {
          padding-top: 2px;
        }

      `}</style>
    </a>
  </Link>
)
