import React from 'react';

import BackgroundVideo from './BackgroundVideo';

export default ({ name, surname, avatarUrl, birthday }) => {
  const today = new Date();
  const isTodayBirthDay = today.getDate() === birthday.day && today.getMonth() === birthday.month;

  return (
    <div className="card">
      <div className="header">
        <div className="avatar">
          <img alt={`${name} ${surname} avatar`} src={avatarUrl} />
        </div>
        <h3>
          Benvenuto {name}!
          <span>(Si, quello e' il tuo avatar)</span>
        </h3>
      </div>

      {isTodayBirthDay ? (
        <div className="body">
          <BackgroundVideo />
          <h1>Tanti auguri! 🎉🎉🎉</h1>
          <p>
            Ti auguro di tutto cuore un felice compleanno e filici NaN anni.
          </p>
          <p>
            Per ritirare il tuo regalo, invami uno screenshot di questa pagina.
            Non esisteva una API per automatizzare questa ultima parte del
            processo..
          </p>
        </div>
      ) : (
        <div className="body">
          <h1>Hmm...</h1>
          <p>
            Sembra che vuoi vedere i tuoi auguri di compleanno ma non &egrave;
            il tuo compleanno...
          </p>
          <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
            Guarda gli auguri di nuovo
          </a>
        </div>
      )}

      <style jsx>{`

        .card {
          padding: 25px;
          background-color: white;
          box-shadow: 0 2px 8px rgba(0, 0, 0, .15);
          border-radius: 2px;
          color: #333;
          max-width: 500px;
        }

        .header {
          display: flex;
          align-items: center;
          border-bottom: 1px solid rgba(0, 0, 0, .1);
          padding-bottom: 15px;
          margin-bottom: 25px;
        }

        .avatar {
          border-radius: 50%;
          height: 50px;
          width: 50px;
          overflow: hidden;
          margin-right: 20px;
        }

        .avatar img {
          width: 100%;
          object-fit: contain;
        }

        h3 {
          font-size: 15px;
          font-weight: 600;
        }


        h3 span {
          display: block;
          font-size: 12px;
          line-height: 1.5;
          color: rgba(0, 0, 0, .6);
          font-weight: 500;
        }

        h1 {
          margin-top: 0;
          margin-bottom: 0.25em;
        }

        h1 span {
          display: block;
          line-height: 1.5;
          font-size: 18px;
          font-weight: 300;
          color: rgba(0, 0, 0, .6);
        }

        p {
          margin-top: 0;
          margin-bottom: 1em;
          color: rgba(0, 0, 0, .7);
          line-height: 1.5;
        }

        a {
          display: block;
          margin-top: 25px;
          margin-bottom: 15px;
          width: 100%;
          text-align: center;
          font-size: 15px;
          font-weight: 600;
          letter-spacing: 0.05em;
          text-transform: uppercase;
          color: white;
          text-decoration: none;
          padding: 20px 25px;
          background: linear-gradient(124deg, #ff2400, #e81d1d, #e8b71d, #e3e81d, #1de840, #1ddde8, #2b1de8, #dd00f3, #dd00f3);
          background-size: 1800% 1800%;
          animation: rainbow 18s ease infinite;
          border-radius: 2px;
          text-shadow: 0 2px 2px rgba(0, 0, 0, .2);
        }

        @keyframes rainbow {
          0%{background-position:0% 82%}
          50%{background-position:100% 19%}
          100%{background-position:0% 82%}
        }

      `}</style>
    </div>
  );
}
