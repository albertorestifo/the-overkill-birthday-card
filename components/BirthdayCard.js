import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import Login from './Login';
import Wishes from './Wishes';

const BirthdayCard = ({ data: { loading, me } }) => {
  if (loading) {
    return (
      <h2>Loading</h2>
    );
  }

  if (!me) {
    return <Login />;
  }

  // Ensure the logged-in user is my brother
  if (/claudio/i.test(me.name) && /restifo/i.test(me.surname)) {
    return <Wishes {...me} />
  }

  return (
    <p>
      This is a gift ment for my brother. There is nothing for you to see here.
    </p>
  );
};

const GetMeQuery = gql`
  query GetMe {
    me {
      email
      name
      surname
      birthday {
        year
        month
        day
      }
      avatarUrl
    }
  }
`;

export default graphql(GetMeQuery)(BirthdayCard);
