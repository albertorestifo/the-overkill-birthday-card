const google = require('googleapis');
const cookie = require('cookie');

const redis = require('./utils/redis');

// https://developers.google.com/identity/protocols/googlescopes
const scopes = [
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/user.birthday.read',
  'https://www.googleapis.com/auth/user.emails.read',
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/userinfo.profile',
]

module.exports = (req, res, query) => {
  const oauth2Client = require('./utils/oauth').getClient();

  // If we have a query code, then we're receiving the response from Google
  if (query.code) {

    return oauth2Client.getToken(query.code, function (err, tokens) {
      if (err) {
        res.write(err.toString());
        return res.end();
      }

      redis.set(tokens.access_token, JSON.stringify(tokens));
      const setCookie = cookie.serialize('token', tokens.access_token);

      res.writeHead(302, {
        Location: '/',
        'Set-Cookie': setCookie,
      });

      return res.end();
    });
  }

  // Othrwhise generate the URL and redirect
  const url = oauth2Client.generateAuthUrl({ scope: scopes });

  res.writeHead(302, { Location: url });
  return res.end();
};
