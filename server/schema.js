const { makeExecutableSchema } = require('graphql-tools');

const getUser = require('./getUser');

const typeDefs = `
  type Date {
    year: Int!
    month: Int!
    day: Int!
  }

  type User {
    email: String!
    name: String!
    surname: String!
    birthday: Date!
    avatarUrl: String!
  }

  type Query {
    me: User
  }
`;

const getPrimary = (key, data) => {
  const entries = data[key];
  if (!entries) return {};
  return entries.find(row => row.metadata.primary) || {};
};

const resolvers = {
  Query: {
    me: (root, args, ctx) => ctx.token ? getUser(ctx.token) : null,
  },

  User: {
    email: user => getPrimary('emailAddresses', user).value,
    name: user => getPrimary('names', user).givenName,
    surname: user => getPrimary('names', user).familyName,
    birthday: user => getPrimary('birthdays', user).date,
    avatarUrl: user => getPrimary('photos', user).url,
  },
};

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

module.exports = schema;

