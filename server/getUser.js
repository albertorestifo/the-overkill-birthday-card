const google = require('googleapis');

const redis = require('./utils/redis');

const people = google.people('v1');

const getMe = (client) => {
  return new Promise((resolve, reject) => {
    people.people.get({
      resourceName: 'people/me',
      personFields: 'birthdays,emailAddresses,names,photos',
      auth: client
    }, (err, user) => {
      if (err) return reject(err);
      return resolve(user);
    });
  });
}

async function getUser(token) {
  let credentials = await redis.get(token);
  if (!credentials) return null;

  credentials = JSON.parse(credentials);

  const oauth2Client = require('./utils/oauth').getClient();
  oauth2Client.credentials = credentials;

  return getMe(oauth2Client);
}

module.exports = getUser;
