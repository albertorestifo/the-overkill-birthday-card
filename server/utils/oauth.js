const google = require('googleapis');

const OAuth2 = google.auth.OAuth2;

function getClient() {
  const redirectUrl = 'https://theoverkillbirthdaycard.website/oauth';
  return new OAuth2(
    process.env.CLIENT_ID,
    process.env.CLIENT_SECRET,
    redirectUrl
  );
}

module.exports = {
  getClient,
};
