const { createServer } = require('http')
const { parse } = require('url')
const next = require('next')
const micro = require('micro');
const match = require('micro-route/match');
const { microGraphql } = require('apollo-server-micro');
const cookie = require('cookie');

const oauthHandler = require('./server/oauthHandler');
const schema = require('./server/schema');

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const getToken = (bearer) => {
  const splitted = bearer.split(' ' );
  if (splitted.length === 2) return splitted[1];
  return undefined;
}

const server = micro(async (req, res) => {
  const parsedUrl = parse(req.url, true)
  const { query } = parsedUrl
  const cookies = cookie.parse(req.headers.cookie || '');

  const token = req.headers.authorization && getToken(req.headers.authorization);
  const context = {
    token: cookies.token || token,
  };

  const graphqlHandler = microGraphql({ schema, context });

  if (match(req, '/oauth')) {
    return oauthHandler(req, res, query);
  }

  if (match(req, '/graphql')) {
    return graphqlHandler(req, res);
  }

  return handle(req, res, parsedUrl);
});

app.prepare()
  .then(() => {
    server.listen(port, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    });
  });
